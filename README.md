# Programación Orientada a Objetos Proyecto Segundo Parcial 2020 – 1T

## Integrantes

- Abel Francisco Gómez Campuzano `@abelfgomez` - `afgomez@espol.edu.ec`
- Ivan Ariel González Moreira `@Arielin_gm` - `ivargonz@espol.edu.ec`
- Juan Antonio González Orbe `@anntnzrb` - `juangonz@espol.edu.ec`

## Objetivos

- Diseñar una solución reconociendo los objetos participantes en un problema y
  la relación entre estos
- Implementar una solución en un lenguaje de programación orientado a objetos,
  aprovechando la interfaz gráfica (En JavaFX) y aplicando el concepto de hilos
- Fortalecer el auto aprendizaje y la investigación

## Introducción

Debido al crecimiento poblacional y a la explotación de recursos sin control se
ha empezado a explorar otros planetas en busca de recursos naturales. Uno de
los países que se explora es Marte. Se tiene principal interés en la
exploración de los cráteres.
