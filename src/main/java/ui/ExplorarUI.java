package ui;

import control.*;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import utils.Constantes.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Locale;

import static utils.Constantes.*;


/**
 * Interfaz grafica Explorar
 */
public final
class ExplorarUI {

    public static final short                  IMG_MARTE_WIDTH  = 1190;
    public static final short                  IMG_MARTE_HEIGHT = 625;
    public static       Rover                  bender;
    public static       ArrayList<Crater>      listCrateres;
    public static       ArrayList<Exploracion> listExploracion;
    private static      BorderPane             root;
    private static      Label                  lblCursorPos;
    private static      Label                  lblInfoCrater;
    private static      TextField              txtFComandosInput;
    private static      TextArea               txtAComandosOutput;


    /* Prevenir instanciamiento */
    private
    ExplorarUI() {}

    /**
     * Muestra la escena con la opcion del menu 'Explorar'
     */
    static
    void show()
    {
        /* ---------------------------------------------------------------------
         * ROOT
         * ------------------------------------------------------------------ */
        root = new BorderPane();
        root.setStyle(
            "-fx-background-color: " + Ventana.EXPLORAR.getColorFondo());
        /* Inicializacion de lista de Crateres */
        try {
            listCrateres = CraterData.cargarCrater();
            listExploracion = ExploracionData.leerArchivoCrater();
        } catch (final IOException ioEx) {
            ErrorUI.show();
            SYS_ERR_MSG(ioEx.getMessage());
        }

        /* ---------------------------------------------------------------------
         * PANEL SUPERIOR/INFERIOR/DERECHO/IZQUIERDO
         * ------------------------------------------------------------------ */
        try {
            root.setTop(seccSuperior());
            root.setBottom(seccInferior());
            root.setRight(seccDerecha());
            root.setCenter(seccCentro());

            /* -----------------------------------------------------------------
             * PROPIEDADES STAGE/VENTANA
             * -------------------------------------------------------------- */
            App.getStage()
               .setScene(new Scene(root, Ventana.EXPLORAR.getDimX(),
                                   Ventana.EXPLORAR.getDimY()));
            App.getStage().setTitle(Ventana.EXPLORAR.getTitulo());
            App.getStage().show();
        } catch (final IOException ioEx) {
            ErrorUI.show();
            SYS_ERR_MSG(ioEx.getMessage());
        }
    }

    /**
     * Seccion Superior
     */
    private static
    Pane seccSuperior() throws FileNotFoundException
    {
        /* ---------------------------------------------------------------------
         * CABECERA
         * ------------------------------------------------------------------ */
        final var imgTitu = new Image(
            new FileInputStream(Ruta.IMG.getRuta() + "panel_control.png"), 200,
            165, true, true);

        /* ---------------------------------------------------------------------
         * PROPIEDADES
         * ------------------------------------------------------------------ */
        final var imgBox = new ImageView(imgTitu);
        final var panelSuperior = new HBox(imgBox);
        panelSuperior.setAlignment(Pos.CENTER);

        return panelSuperior;
    }

    /**
     * Seccion Inferior
     */
    private static
    Pane seccInferior()
    {
        /* ---------------------------------------------------------------------
         * BOTONES
         * ------------------------------------------------------------------ */
        /* Boton salir */
        final var btnSalir = new Button("Salir");
        btnSalir.setStyle(
            "-fx-background-color: lightcoral; -fx-text-fill: white;");
        /* Boton regresar (Lobby) */
        final var btnRegresar = new Button("Regresar");
        btnRegresar.setStyle(
            "-fx-background-color: forestgreen; -fx-text-fill: white;");
        /* Posicion Cursor */
        lblCursorPos = new Label();
        lblCursorPos.setStyle("-fx-font-weight: bold; -fx-text-fill: gold;");
        lblInfoCrater = new Label();
        lblInfoCrater.setStyle(
            "-fx-font-weight: bold; -fx-text-fill: greenyellow;");

        /* ---------------------------------------------------------------------
         * PROPIEDADES
         * ------------------------------------------------------------------ */
        final var panelInferior =
            new AnchorPane(btnSalir, btnRegresar, lblInfoCrater, lblCursorPos);
        AnchorPane.setRightAnchor(btnSalir, 5.0);
        AnchorPane.setRightAnchor(btnRegresar, 55.0);
        AnchorPane.setLeftAnchor(lblCursorPos, 0.0);
        AnchorPane.setLeftAnchor(lblInfoCrater, 150.0);

        /* ---------------------------------------------------------------------
         * EVENTOS
         * ------------------------------------------------------------------ */
        btnSalir.setOnAction(new APP_EXIT_EV());
        btnRegresar.setOnAction(e -> LobbyUI.show());

        return panelInferior;
    }

    /**
     * Seccion Derecha
     */
    private static
    Pane seccDerecha()
    {
        /* ---------------------------------------------------------------------
         * COMANDOS
         * ------------------------------------------------------------------ */
        /* Input */
        final var lblComandosInput = new Label("Ingrese comandos:");
        txtFComandosInput = new TextField();
        txtFComandosInput.setMaxSize(150, 50);
        txtFComandosInput.setPromptText("comando [args]");
        /* Output */
        final var lblComandosOutput = new Label("Comandos ingresados:");
        txtAComandosOutput = new TextArea();
        txtAComandosOutput.setDisable(true);
        txtAComandosOutput.setMaxSize(150, 150);
        txtAComandosOutput.setPromptText("Listado de comandos utilizados...");

        /* ---------------------------------------------------------------------
         * BOTONES
         * ------------------------------------------------------------------ */
        /* Boton Limpiar */
        final var btnLimpiar = new Button("Limpiar");
        btnLimpiar.setStyle(
            "-fx-background-color: darkslateblue; -fx-text-fill: white;");
        /* Boton Color Picker */
        final var colorPicker = new ColorPicker(Color.NAVY);

        /* ---------------------------------------------------------------------
         * LABELS
         * ------------------------------------------------------------------ */
        final var lblAyuda =
            new Label("Comandos disponibles:\n(Valores positivos) ");
        lblAyuda.setStyle("-fx-font-weight: bold");
        final var ayudaTxt = new Text(Mensaje.AYUDA.getMsg());

        final var panelDerecho =
            new VBox(10, lblComandosInput, txtFComandosInput, lblComandosOutput,
                     txtAComandosOutput, btnLimpiar, lblAyuda, ayudaTxt,
                     colorPicker);
        panelDerecho.setAlignment(Pos.CENTER);

        /* ---------------------------------------------------------------------
         * EVENTOS
         * ------------------------------------------------------------------ */
        btnLimpiar.setOnAction(e -> {
            txtFComandosInput.setText(null);
            txtAComandosOutput.setText(null);
        });
        colorPicker.setOnAction(ev -> CAMBIAR_CP(root, colorPicker));

        return panelDerecho;
    }

    /**
     * Seccion Centro
     */
    private static
    Pane seccCentro() throws IOException
    {
        /* ---------------------------------------------------------------------
         * Imagen Crater Marte
         * ------------------------------------------------------------------ */
        final var imgMarte = new Image(
            new FileInputStream(Ruta.IMG.getRuta() + "superficie_marte.png"),
            IMG_MARTE_WIDTH, IMG_MARTE_HEIGHT, false, true);
        final var imgBox = new ImageView(imgMarte);
        imgBox.setOnMouseMoved(e -> lblCursorPos.setText(
            "X: " + e.getX() + " " + "Y: " + e.getY()));

        /* ---------------------------------------------------------------------
         * PROPIEDADES
         * ------------------------------------------------------------------ */
        final var panelCentro = new Pane(imgBox);

        for (final var crat : listCrateres) {
            /* Generar crateres (circulos) */
            panelCentro.getChildren().add(crat.getCirculoCrater());

            for (final var explo : listExploracion) {
                if (explo.getCrater().equals(crat)) {
                    crat.getCirculoCrater().setFill(Color.MEDIUMTURQUOISE);
                }
            }
            /* Evento "click" en circulo */
            crat.getCirculoCrater().setOnMouseClicked(e -> {
                lblInfoCrater.setText("Info Crater => " + crat);
                for (final var explo : listExploracion) {
                    if (explo.getCrater().equals(crat)) {
                        lblInfoCrater.setText("Info Explo => " + explo);
                    }
                }
            });
        }

        /* ---------------------------------------------------------------------
         * ROVER
         * ------------------------------------------------------------------ */
        bender = new Rover();
        final var imgRover = bender.getImgRoverBox();
        txtFComandosInput.setOnKeyPressed(keyEv -> {
            /* Obtener el comando ingresado */
            final String[] argv;
            final var cmd =
                txtFComandosInput.getText().toLowerCase(Locale.ENGLISH);

            /* Ejecutar "cmd" a partir de Enter */
            if (keyEv.getCode() == KeyCode.ENTER) {
                if ("avanzar".equals(cmd)) {
                    bender.avanzar();
                    txtAComandosOutput.appendText(cmd + "\n");
                } else if (cmd.contains("girar")) {
                    argv = cmd.split(":");
                    bender.girarProgresivo(Double.parseDouble(argv[1]));
                    txtAComandosOutput.appendText(cmd + "\n");
                } else if (cmd.contains("dirigirse")) {
                    argv = cmd.split(":")[1].split(",");
                    bender.dirigirse(Double.parseDouble(argv[0]),
                                     Double.parseDouble(argv[1]));
                    txtAComandosOutput.appendText(cmd + "\n");
                } else if ("censar".equals(cmd)) {
                    for (final var crat : listCrateres) {
                        if (PlanificarUI.calcularDistancia(
                            bender.getImgRoverBox().getLayoutX(),
                            bender.getImgRoverBox().getLayoutY(),
                            crat.getLatitud(), crat.getLonguitud()) <=
                            crat.getRadio()) {
                            bender.censar(crat);
                            crat.getCirculoCrater()
                                .setFill(Color.MEDIUMTURQUOISE);
                            txtAComandosOutput.appendText("Censado con exito");
                        }
                    }
                }
            }
        });

        /* Agregar Rover al Layout */
        panelCentro.getChildren().add(imgRover);

        return panelCentro;
    }
}