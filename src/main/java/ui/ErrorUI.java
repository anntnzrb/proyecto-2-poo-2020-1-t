package ui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import utils.Constantes.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;


/**
 * Interfaz grafica Error
 */
public final
class ErrorUI {

    private static VBox root;

    /* Prevenir instancimiento */
    private
    ErrorUI() {}

    public static
    void show()
    {
        /* ---------------------------------------------------------------------
         * CABECERA
         * ------------------------------------------------------------------ */
        final var lblCabecera = new Label("ERROR: ");
        lblCabecera.setStyle("-fx-font-weight: bold");

        /* ---------------------------------------------------------------------
         * BOTONES
         * ------------------------------------------------------------------ */
        /* Boton salir */
        final var btnSalir = new Button("Salir");
        btnSalir.setStyle(
            "-fx-background-color: lightcoral; -fx-text-fill: white;");

        /* ---------------------------------------------------------------------
         * TEXTO
         * ------------------------------------------------------------------ */
        final var cuerpoTxt = new Text(Mensaje.ERROR.getMsg());
        cuerpoTxt.setFont(new Font(14));

        /* ---------------------------------------------------------------------
         * IMAGEN
         * ------------------------------------------------------------------ */
        Image imgError = null;
        try {
            imgError = new Image(
                new FileInputStream(Ruta.IMG.getRuta() + "error_alert.png"),
                300, 300, true, true);
        } catch (final FileNotFoundException e) {
            e.printStackTrace();
        }
        final var imgBox = new ImageView(imgError);
        final var imgHBox = new HBox(imgBox);

        /* ---------------------------------------------------------------------
         * EVENTOS
         * ------------------------------------------------------------------ */
        btnSalir.setOnAction(new APP_EXIT_EV());

        /* ---------------------------------------------------------------------
         * LAYOUT
         * ------------------------------------------------------------------ */
        root = new VBox(10, imgHBox, lblCabecera, cuerpoTxt, btnSalir);
        root.setStyle("-fx-background-color: " + Ventana.ERROR.getColorFondo());
        root.setAlignment(Pos.CENTER);
        imgHBox.setAlignment(Pos.CENTER);

        /* ---------------------------------------------------------------------
         * PROPIEDADES STAGE/VENTANA
         * ------------------------------------------------------------------ */
        App.getStage()
           .setScene(new Scene(root, Ventana.ERROR.getDimX(),
                               Ventana.ERROR.getDimY()));
        App.getStage().centerOnScreen();
        App.getStage().setTitle(Ventana.ERROR.getTitulo());
        App.getStage().show();
    }
}