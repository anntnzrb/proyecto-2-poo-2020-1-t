package ui;

import control.Exploracion;
import control.ExploracionData;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import utils.Constantes;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;


final
class ReportesUI {

    static final   TableView tabla      = new TableView();
    static final   TextField txtMineral = new TextField();
    private static VBox      root;
    private static VBox      seccionTabla;

    /* Prevenir instanciamiento */
    private
    ReportesUI() {}

    /**
     * Muestra la escena con la opcion del menu 'Ver Reportes'
     */
    static
    void show()
    {
        /* ---------------------------------------------------------------------
         * ROOT
         * ------------------------------------------------------------------ */
        root = new VBox();
        seccionTabla = new VBox();

        root.setStyle("-fx-background-color: " +
                      Constantes.Ventana.REPORTES.getColorFondo());

        /* Cargar secciones */
        seccionGrid();
        seccionReporte(getExploracion());
        seccionbutton();


        /* ---------------------------------------------------------------------
         * PROPIEDADES STAGE/VENTANA
         * ------------------------------------------------------------------ */
        App.getStage()
           .setScene(new Scene(root, Constantes.Ventana.REPORTES.getDimX(),
                               Constantes.Ventana.REPORTES.getDimY()));
        App.getStage().setTitle(Constantes.Ventana.REPORTES.getTitulo());
        App.getStage().show();
    }

    /**
     * Seccion Reportes
     */
    private static
    void seccionGrid()
    {
        final var filtroBox = new GridPane();
        filtroBox.setHgap(15);
        filtroBox.setVgap(15);

        final var txtFechaIni = new TextField();
        final var btnLimpiar = new Button("Limpiar");
        btnLimpiar.setStyle(
            "-fx-background-color: darkslateblue; -fx-text-fill: white;");
        txtFechaIni.setMaxSize(400, 70);
        filtroBox.addRow(0, new Label("Fecha Inicio"), txtFechaIni);

        final var txtFechaFin = new TextField();
        final var btnFiltrar = new Button("Filtrar");
        btnFiltrar.setStyle(
            "-fx-background-color: darkslateblue; -fx-text-fill: white;");
        txtFechaFin.setMaxSize(400, 70);
        filtroBox.addRow(1, new Label("Fecha Fin"), txtFechaFin, btnFiltrar);


        txtMineral.setMaxSize(400, 70);
        filtroBox.addRow(2, new Label("Minerales"), txtMineral, btnLimpiar);

        btnLimpiar.setOnAction(e -> {
            txtFechaIni.setText(null);
            txtFechaFin.setText(null);
            txtMineral.setText(null);
        });

        btnFiltrar.setOnAction(f -> {
            if (!txtFechaIni.getText().isBlank() &
                !txtFechaFin.getText().isBlank()) {
                seccionReporte(filtrar(LocalDate.parse(txtFechaIni.getText()),
                                       LocalDate.parse(txtFechaFin.getText())));
            } else if (!txtFechaIni.getText().isBlank()) {
                seccionReporte(
                    filtrar(LocalDate.parse(txtFechaIni.getText()), "inicio"));
            } else if (!txtFechaFin.getText().isBlank()) {
                seccionReporte(
                    filtrar(LocalDate.parse(txtFechaFin.getText()), "fin"));
            } else {
                seccionReporte(getExploracion());
            }
        });

        root.getChildren().add(filtroBox);
        root.setSpacing(15);
    }

    /**
     * Seccion Reportes
     */
    private static
    void seccionReporte(final ObservableList<Exploracion> tabla_exploracion)
    {
        Collections.sort(tabla_exploracion);
        final var fechaExpCol = new TableColumn<>("Fecha Exploracion");
        fechaExpCol.setMinWidth(200);
        fechaExpCol.setCellValueFactory(
            new PropertyValueFactory<>("fecha_exp"));

        final var nombreCol = new TableColumn<>("Crater");
        nombreCol.setMinWidth(300);
        nombreCol.setCellValueFactory(new PropertyValueFactory<>("crater"));

        final var mineralesCol = new TableColumn<>("Minerales");
        mineralesCol.setMinWidth(400);
        mineralesCol.setCellValueFactory(
            new PropertyValueFactory<>("min_encontrados"));

        final FilteredList<Exploracion> datos_filtrados =
            new FilteredList<>(tabla_exploracion, b -> true);
        txtMineral.textProperty()
                  .addListener((observable, oldValue, newValue) -> {
                      datos_filtrados.setPredicate(exploracion -> {
                          if (newValue == null || newValue.isEmpty()) {
                              return true;
                          }
                          final String lowerfilter = newValue.toLowerCase();
                          return exploracion.getMin_encontrados()
                                            .toLowerCase()
                                            .indexOf(lowerfilter) != -1;
                      });
                  });
        final SortedList<Exploracion> datos_sorted =
            new SortedList<>(datos_filtrados);
        datos_sorted.comparatorProperty().bind(tabla.comparatorProperty());
        tabla.getColumns().clear();
        tabla.setItems(datos_sorted);
        tabla.getColumns().addAll(fechaExpCol, nombreCol, mineralesCol);
        seccionTabla.getChildren().add(tabla);

        root.getChildren().addAll(seccionTabla);
    }

    static
    ObservableList<Exploracion> getExploracion()
    {
        final ObservableList<Exploracion> exploraciones =
            FXCollections.observableArrayList();
        try {
            exploraciones.addAll(ExploracionData.leerArchivoCrater());
        } catch (final IOException ex) {
            ex.printStackTrace();
        }

        return exploraciones;
    }
    private static
    void seccionbutton()
    {

        final var btnSalir = new Button("Salir");
        btnSalir.setStyle(
            "-fx-background-color: lightcoral; -fx-text-fill: white;");
        /* Boton regresar (Lobby) */
        final var btnRegresar = new Button("Regresar");
        btnRegresar.setStyle(
            "-fx-background-color: forestgreen; -fx-text-fill: white;");

        btnSalir.setOnAction(new Constantes.APP_EXIT_EV());
        btnRegresar.setOnAction(e -> LobbyUI.show());
        final Pane seccionButton = new HBox(10, btnRegresar, btnSalir);
        root.getChildren().add(seccionButton);
    }
    static
    ObservableList<Exploracion> filtrar(final LocalDate f_ini,
                                        final LocalDate f_fin)
    {

        final ObservableList<Exploracion> exploraciones = getExploracion();
        final ObservableList<Exploracion> exploraciones_filtro =
            FXCollections.observableArrayList();
        for (final Exploracion expl : exploraciones) {
            if (expl.getFecha_exp().getDayOfYear() <= f_fin.getDayOfYear() &
                expl.getFecha_exp().getDayOfYear() >= f_ini.getDayOfYear()) {
                exploraciones_filtro.add(expl);
            }
        }
        return exploraciones_filtro;
    }
    static
    ObservableList<Exploracion> filtrar(final LocalDate fecha,
                                        final String parametro)
    {
        final ObservableList<Exploracion> exploraciones = getExploracion();
        final ObservableList<Exploracion> exploraciones_filtro =
            FXCollections.observableArrayList();
        if (parametro.toLowerCase().equals("inicio")) {
            for (final Exploracion expl : exploraciones) {
                if (expl.getFecha_exp().getDayOfYear() >=
                    fecha.getDayOfYear()) {
                    exploraciones_filtro.add(expl);
                }
            }
        }
        if (parametro.toLowerCase().equals("fin")) {
            for (final Exploracion expl : exploraciones) {
                if (expl.getFecha_exp().getDayOfYear() <=
                    fecha.getDayOfYear()) {
                    exploraciones_filtro.add(expl);
                }
            }
        }
        return exploraciones_filtro;
    }
    /**
     * new Exploracion(new Crater("1", "Pancho", 123.45, 476.8, 70),
     LocalDate.now()));
     exploraciones.add(
     new Exploracion(new Crater("2", "Arielin", 143.45, 276.8, 93),
     LocalDate.of(2020, Month.MARCH, 30)));
     exploraciones.add(
     new Exploracion(new Crater("3", "JUANCHO", 143.45, 276.8, 93),
     LocalDate.now()));
     exploraciones.add(
     new Exploracion(new Crater("4", "SANCHO", 143.45, 276.8, 93),
     LocalDate.now()));
     exploraciones.add(
     new Exploracion(new Crater("5", "ALIENX", 143.45, 276.8, 93),
     LocalDate.now()));
     */

}
