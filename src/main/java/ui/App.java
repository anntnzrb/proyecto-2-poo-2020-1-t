package ui;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import utils.Constantes;


/**
 * @author Abel Francisco Gomez Campuzano
 * @author Ivan Ariel Gonzalez Moreira
 * @author Juan Antonio Gonzalez Orbe
 */
public final
class App extends Application {

    private static final boolean DEBUG_ERRORUI = false;
    private static       Stage   stage;

    /**
     * Metodo para ejecturar aplicacion (NO UTILIZAR)
     * Ejecutar mediante clase 'main' {@link Launcher}
     *
     * @param args argumentos pasados desde consola (no empleado)
     */
    public static
    void main(final String... args)
    {
        launch();
    }
    /* -------------------------------------------------------------------------
     * Getters & Setters
     * ---------------------------------------------------------------------- */
    static
    Stage getStage() { return stage; }

    @Override
    public
    void start(final Stage primaryStage)
    {
        /* ---------------------------------------------------------------------
         * PROPIEDADES STAGE/VENTANA
         * ------------------------------------------------------------------ */
        stage = primaryStage;
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.getIcons()
             .add(new Image(
                 "file:" + Constantes.Ruta.IMG.getRuta() + "rover_icon.png"));

        /* ---------------------------------------------------------------------
         * EJECUTAR VENTANAS
         * ------------------------------------------------------------------ */
        try {
            /* TRUE -> Debug ErrorUI */
            if (DEBUG_ERRORUI) { throw new Exception("DEBUG ERROR UI"); }

            /* Ejecutar UI Lobby */
            LobbyUI.show();
        } catch (final Exception ex) {
            /* Imprimir error en consola */
            ex.printStackTrace();
            /* Ejecutar UI Error */
            ErrorUI.show();
        }
    }
}