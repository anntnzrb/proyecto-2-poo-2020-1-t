package ui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import utils.Constantes.Contacto;
import utils.Constantes.Ventana;

import static utils.SystemInfo.javaVersion;
import static utils.SystemInfo.javafxVersion;


/**
 * Interfaz grafica Atribuciones
 */
final
class AtribucionesUI {
    /* -------------------------------------------------------------------------
     * ATRIBUTOS
     * ---------------------------------------------------------------------- */
    /* root & Layout(s) */
    private static Stage stage;

    /* Prevenir instanciamiento */
    private
    AtribucionesUI() {}

    /**
     * Muestra la ventana emergente de Atribuciones
     */
    static
    void show()
    {
        /* ---------------------------------------------------------------------
         * LABELS
         * ------------------------------------------------------------------ */
        /* Cabecera */
        final var lblCabecera = new Label("Atribuciones");
        lblCabecera.setStyle("-fx-font-weight: bold");
        /* Autor X/Y/Z */
        final var lblAutorX = new Label(Contacto.AUTORX.getNombre());
        final var lblAutorY = new Label(Contacto.AUTORY.getNombre());
        final var lblAutorZ = new Label(Contacto.AUTORZ.getNombre());
        /* Info */
        final var lblInfo = new Label(
            "Usando Java " + javaVersion() + " mediante Java " + "FX " +
            javafxVersion());
        lblInfo.setStyle("-fx-font-weight: bold");

        /* ---------------------------------------------------------------------
         * BOTONES
         * ------------------------------------------------------------------ */
        /* Boton salir */
        final var btnSalir = new Button("Salir");
        btnSalir.setStyle(
            "-fx-background-color: lightcoral; -fx-text-fill: white;");

        /* ---------------------------------------------------------------------
         * LAYOUT
         * ------------------------------------------------------------------ */
        final var root =
            new VBox(10, lblCabecera, lblAutorX, lblAutorY, lblAutorZ, lblInfo,
                     btnSalir);
        root.setAlignment(Pos.CENTER);
        root.setStyle(
            "-fx-background-color: " + Ventana.ATRIBUCIONES.getColorFondo());

        /* ---------------------------------------------------------------------
         * EVENTOS
         * ------------------------------------------------------------------ */
        btnSalir.setOnAction(e -> stage.close());

        /* ---------------------------------------------------------------------
         * PROPIEDADES STAGE/VENTANA
         * ------------------------------------------------------------------ */
        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setTitle(Ventana.ATRIBUCIONES.getTitulo());
        stage.centerOnScreen();
        stage.setScene(new Scene(root, Ventana.ATRIBUCIONES.getDimX(),
                                 Ventana.ATRIBUCIONES.getDimY()));
        stage.showAndWait();
    }
}