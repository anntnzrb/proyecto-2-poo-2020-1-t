package ui;

import control.Crater;
import control.CraterData;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import utils.Constantes.Ventana;

import java.io.IOException;
import java.util.*;

import static java.lang.StrictMath.pow;
import static java.lang.StrictMath.sqrt;
import static utils.Constantes.SYS_ERR_MSG;


/**
 * Interfaz grafica Atribuciones
 */
public final
class PlanificarUI {

    public static  List<Crater> listCrateres;
    /* -------------------------------------------------------------------------
     * ATRIBUTOS
     * ---------------------------------------------------------------------- */
    /* root & Layout(s) */
    private static Stage        stage;

    /* Prevenir instanciamiento */
    private
    PlanificarUI() {}

    /**
     * Muestra la ventana emergente de Atribuciones
     */
    static
    void show()
    {
        /* Inicializacion de lista de Crateres */
        try {
            listCrateres = CraterData.cargarCrater();
        } catch (final IOException ioEx) {
            ErrorUI.show();
            SYS_ERR_MSG(ioEx.getMessage());
        }

        /* ---------------------------------------------------------------------
         * LABELS
         * ------------------------------------------------------------------ */
        /* Cabecera */
        final var lblCabecera = new Label("Planificar Ruta");
        lblCabecera.setStyle("-fx-font-weight: bold");
        /* Autor X/Y/Z */
        final var lblAutorX = new Label("Ingrese Crateres a Planificar");
        final var txtFComandosInput = new TextField();
        txtFComandosInput.setMaxSize(150, 50);
        txtFComandosInput.setPromptText("Mead,Hansberry,Meither");

        /* Info */
        final var lblInfo = new Label();
        lblInfo.setStyle("-fx-font-weight: bold");

        /* ---------------------------------------------------------------------
         * BOTONES
         * ------------------------------------------------------------------ */
        /* Boton salir */
        final var btnSalir = new Button("Salir");
        btnSalir.setStyle(
            "-fx-background-color: lightcoral; -fx-text-fill: white;");

        /* ---------------------------------------------------------------------
         * LAYOUT
         * ------------------------------------------------------------------ */
        final var root =
            new VBox(10, lblCabecera, lblAutorX, txtFComandosInput, lblInfo,
                     btnSalir);
        root.setAlignment(Pos.CENTER);
        root.setStyle(
            "-fx-background-color: " + Ventana.PLANIFICAR.getColorFondo());

        /* ---------------------------------------------------------------------
         * EVENTOS
         * ------------------------------------------------------------------ */
        final var listaCraterFiltrado = new ArrayList<Crater>();
        btnSalir.setOnAction(e -> stage.close());
        txtFComandosInput.setOnKeyPressed(keyEv -> {
            if (keyEv.getCode() == KeyCode.ENTER) {
                final String[] part;
                part = txtFComandosInput.getText().split(",");
                for (final var cr : listCrateres) {
                    for (final var st : part) {
                        if (cr.getNombre().equalsIgnoreCase(st)) {
                            listaCraterFiltrado.add(cr);
                        }
                    }
                }

                double dlo = 0;
                double dla = 0;
                final var listaImprimir = new ArrayList<Crater>();
                while (!listaCraterFiltrado.isEmpty()) {
                    final List<Double> distancia = new ArrayList<>();
                    for (final var cr : listaCraterFiltrado) {
                        distancia.add(
                            calcularDistancia(dla, dlo, cr.getLatitud(),
                                              cr.getLonguitud()));
                    }
                    final int indx =
                        distancia.indexOf(Collections.min(distancia));
                    final Crater c = listaCraterFiltrado.get(indx);
                    listaImprimir.add(c);
                    listaCraterFiltrado.remove(indx);
                    dlo = c.getLonguitud();
                    dla = c.getLatitud();
                }
                var abel = "";
                short elhermosojuan = 0;
                for (final Crater e : listaImprimir) {
                    abel += ++elhermosojuan + "." + e.getNombre() + " ";
                }
                lblInfo.setText(abel);
            }
        });

        /* ---------------------------------------------------------------------
         * PROPIEDADES STAGE/VENTANA
         * ------------------------------------------------------------------ */
        stage = new Stage();
        stage.setResizable(false);
        stage.setTitle(Ventana.PLANIFICAR.getTitulo());
        stage.centerOnScreen();
        stage.setScene(new Scene(root, Ventana.PLANIFICAR.getDimX(),
                                 Ventana.PLANIFICAR.getDimY()));
        stage.showAndWait();
    }

    public static
    double calcularDistancia(final double posRovX, final double posRovY,
                             final double pfX, final double pfY)
    {
        final double diffLat = posRovX - pfX;
        final double diffLongi = posRovY - pfY;


        return sqrt(pow(diffLat, 2) + pow(diffLongi, 2));
    }
}
