package ui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import utils.Constantes;
import utils.Constantes.Ventana;

import java.time.LocalDate;

import static utils.Constantes.CAMBIAR_CP;


/**
 * Interfaz grafica Lobby
 */
final
class LobbyUI {

    private static BorderPane root;

    /* Prevenir instanciamiento */
    private
    LobbyUI() {}

    static
    void show()
    {
        /* ---------------------------------------------------------------------
         * ROOT
         * ------------------------------------------------------------------ */
        root = new BorderPane();
        root.setStyle(
            "-fx-background-color: " + Ventana.EXPLORAR.getColorFondo());

        /* ---------------------------------------------------------------------
         * PANEL SUPERIOR/INFERIOR/DERECHO/IZQUIERDO
         * ------------------------------------------------------------------ */
        root.setTop(seccSuperior());
        root.setBottom(seccInferior());
        root.setCenter(seccCentro());

        /* ---------------------------------------------------------------------
         * PROPIEDADES STAGE/VENTANA
         * ------------------------------------------------------------------ */
        App.getStage()
           .setScene(new Scene(root, Ventana.LOBBY.getDimX(),
                               Ventana.LOBBY.getDimY()));
        App.getStage().setTitle(Ventana.LOBBY.getTitulo());
        App.getStage().show();
    }

    /**
     * Seccion Superior
     */
    private static
    Pane seccSuperior()
    {
        /* ---------------------------------------------------------------------
         * TITULOS
         * ------------------------------------------------------------------ */
        final var txtTitulo = new Text("Proyecto II - 2020-1T - G5");
        txtTitulo.setFill(Color.TEAL);
        txtTitulo.setFont(Font.font("Bookman Old Style", FontWeight.BOLD, 50));

        /* ---------------------------------------------------------------------
         * LAYOUT
         * ------------------------------------------------------------------ */
        final var panelSuperior = new FlowPane(txtTitulo);
        panelSuperior.setAlignment(Pos.CENTER);

        return panelSuperior;
    }

    /**
     * Seccion Inferior
     */
    private static
    Pane seccInferior()
    {
        /* ---------------------------------------------------------------------
         * TITULOS
         * ------------------------------------------------------------------ */
        final var lblFecha = new Label("Fecha: " + LocalDate.now());

        /* ---------------------------------------------------------------------
         * LAYOUT
         * ------------------------------------------------------------------ */
        final var panelInferior = new AnchorPane(lblFecha);
        AnchorPane.setLeftAnchor(lblFecha, 0.0);

        return panelInferior;
    }

    /**
     * Seccion Centro
     */
    private static
    Pane seccCentro()
    {
        /* ---------------------------------------------------------------------
         * BOTONES
         * ------------------------------------------------------------------ */
        /* Boton explorar */
        final var btnExplorar = new Button("Explorar");
        btnExplorar.setStyle(
            "-fx-background-color: forestgreen; -fx-text-fill: white;");
        /* Boton Planificar */
        final var btnPlanificar = new Button("Planificar rutas");
        btnPlanificar.setStyle(
            "-fx-background-color: mediumseagreen; -fx-text-fill: white;");
        /* Boton Reportes */
        final var btnReportes = new Button("Ver Reportes");
        btnReportes.setStyle(
            "-fx-background-color: darkkhaki; -fx-text-fill: white;");
        /* Boton salir */
        final var btnSalir = new Button("Salir");
        btnSalir.setStyle(
            "-fx-background-color: lightcoral; -fx-text-fill: white;");
        /* Boton Atribuciones */
        final var btnAtribuciones = new Button("Atribuciones");
        btnAtribuciones.setStyle(
            "-fx-background-color: gold; -fx-text-fill: grey;");
        /* Boton Color Picker */
        final var colorPicker = new ColorPicker(Color.NAVY);

        /* ---------------------------------------------------------------------
         * LAYOUT
         * ------------------------------------------------------------------ */
        /* Cargar Root */
        final var panelCentro =
            new VBox(15, btnExplorar, btnPlanificar, btnReportes, btnSalir,
                     btnAtribuciones, colorPicker);
        panelCentro.setAlignment(Pos.CENTER);

        /* ---------------------------------------------------------------------
         * EVENTOS
         * ------------------------------------------------------------------ */
        btnExplorar.setOnAction(e -> ExplorarUI.show());
        btnPlanificar.setOnAction(e -> PlanificarUI.show());
        btnReportes.setOnAction(e -> ReportesUI.show());
        btnSalir.setOnAction(new Constantes.APP_EXIT_EV());
        btnAtribuciones.setOnAction(ev -> AtribucionesUI.show());
        colorPicker.setOnAction(ev -> CAMBIAR_CP(root, colorPicker));

        return panelCentro;
    }
}