package ui;


/**
 * @author Abel Francisco Gomez Campuzano
 * @author Ivan Ariel Gonzalez Moreira
 * @author Juan Antonio Gonzalez Orbe
 */
public final
class Launcher {

    /**
     * Metodo para ejecturar aplicacion
     *
     * @param args argumentos pasados desde consola (no empleado)
     */
    public static
    void main(final String... args)
    {
        App.main(args);
    }
}
