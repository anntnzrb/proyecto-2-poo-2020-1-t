package utils;

public
enum SystemInfo {
    ;

    /**
     * Obtiene la version de Java que se esta empleando al ejecutar el programa
     *
     * @return version de Java
     */
    public static
    String javaVersion()
    {
        return System.getProperty("java.version");
    }

    /**
     * Obtiene la version de JavaFX que se esta empleando al ejecutar el
     * programa
     *
     * @return version de JavaFX
     */
    public static
    String javafxVersion()
    {
        return System.getProperty("javafx.version");
    }
}