package utils;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ColorPicker;


/**
 * Clase/Enum de utilidades empleado en distintas clases del proyecto
 */
public
enum Constantes {
    ;

    /* -------------------------------------------------------------------------
     * METODOS
     * ---------------------------------------------------------------------- */
    public static
    void SYS_ERR_MSG(final String errMsg)
    {
        System.out.println("-".repeat(44));
        System.err.println("Ha ocurrido un error inesperado...");
        System.err.println("Descripcion: ");
        System.err.println(errMsg);
        System.out.println("-".repeat(44));
    }

    public static
    void CAMBIAR_CP(final Node node, final ColorPicker colorPicker)
    {
        node.setStyle("-fx-background-color: #" +
                      String.valueOf(colorPicker.getValue()).substring(2, 8));
    }

    /* -------------------------------------------------------------------------
     * RUTAS
     * ---------------------------------------------------------------------- */
    public
    enum Ruta {
        IMG("src/main/resources/imgs/"),
        INFO("src/main/resources/info/");

        private final String ruta;

        Ruta(final String ruta)
        {
            this.ruta = ruta;
        }

        public
        String getRuta() { return ruta; }
    }


    /* -------------------------------------------------------------------------
     * CONTACTO
     * ---------------------------------------------------------------------- */
    public
    enum Contacto {
        AUTORX("Juan Antonio González Orbe", "juangonz@espol.edu.ec"),
        AUTORY("Ivan Ariel González Moreira", "ivanrgonz@espol.edu.ec"),
        AUTORZ("Abel Francisco Gomez Campuzano", "afgomez@espol.edu.ec");

        private final String nombre, mail;

        Contacto(final String nombre, final String mail)
        {
            this.nombre = nombre;
            this.mail = mail;
        }

        public
        String getNombre() { return nombre; }

        public
        String getMail() { return mail; }
    }


    /* -------------------------------------------------------------------------
     * VENTANAS
     * ---------------------------------------------------------------------- */
    public
    enum Ventana {
        LOBBY(1375, 700, "(Lobby) Proyecto II - POO 2020-1T - G5", "slateblue"),
        EXPLORAR(LOBBY.dimX, LOBBY.dimY,
                 "(Explorar) Proyecto II - POO " + "2020-1T - G5",
                 "lightslategray"),
        REPORTES(LOBBY.dimX, LOBBY.dimY,
                 "(Reportes) Proyecto II - POO " + "2020-1T - G5", "green"),
        ATRIBUCIONES(400, 250, "(Atribuciones) Proyecto II - G5", "yellow"),
        PLANIFICAR(500, 350, "(Planificar) Proyecto II - G5", "blue"),
        ERROR(600, 500, "Error: Proyecto II G5", "orange");

        private final int dimX, dimY;
        private final String titulo;
        private final String colorFondo;

        Ventana(final int dimX, final int dimY, final String titulo)
        {
            this.dimX = dimX;
            this.dimY = dimY;
            this.titulo = titulo;
            colorFondo = "white";
        }

        Ventana(final int dimX, final int dimY, final String titulo,
                final String colorFondo)
        {
            this.dimX = dimX;
            this.dimY = dimY;
            this.titulo = titulo;
            this.colorFondo = colorFondo;
        }

        public
        String getColorFondo() { return colorFondo; }

        public
        String getTitulo() { return titulo; }

        public
        int getDimX() { return dimX; }

        public
        int getDimY() { return dimY; }
    }


    /* -------------------------------------------------------------------------
     * MENSAJES
     * ---------------------------------------------------------------------- */
    public
    enum Mensaje {
        AYUDA("- avanzar \n- girar:[angulo]\n- dirigirse:[x,y]\n- " +
              "censar\n\nPresione [Enter]\npara enviar el comando"),
        ERROR(String.format("Ha ocurrido un error inesperado\nPor favor " +
                            "contactarse: \n\n[%s] - (%s)\n[%s] - (%s)" +
                            "\n[%s] - (%s)", Contacto.AUTORX.getNombre(),
                            Contacto.AUTORX.getMail(),
                            Contacto.AUTORY.getNombre(),
                            Contacto.AUTORY.getMail(),
                            Contacto.AUTORZ.getNombre(),
                            Contacto.AUTORZ.getMail()));

        private final String msg;

        Mensaje(final String msg)
        {
            this.msg = msg;
        }

        public
        String getMsg() { return msg; }
    }


    /* -------------------------------------------------------------------------
     * EVENTOS
     * ---------------------------------------------------------------------- */
    /* Salir del programa */
    public static
    class APP_EXIT_EV implements EventHandler<ActionEvent> {
        @Override
        public final
        void handle(final ActionEvent event)
        {
            System.out.println("Cerrando programa...");
            Platform.exit();
        }
    }
}