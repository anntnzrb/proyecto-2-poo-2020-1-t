package control;

import utils.Constantes;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;


public
class ExploracionData {

    private static final String      nombreArchiv = "exploracion_info.txt";
    private static       FileWriter  escribir;
    private static       PrintWriter linea;

    /**
     * Este metodo crea y escribe archivo de las exploraciones
     *
     * @param explora
     */
    public static
    void crearArchivoCrater(final Exploracion explora)
    {
        final File archivo =
            new File(Constantes.Ruta.INFO.getRuta() + nombreArchiv);
        final Crater c = explora.getCrater();
        final LocalDate fecha = explora.getFecha_exp();
        final String minerales = explora.getMin_encontrados();
        final String ide = c.getId();
        final String nombre = c.getNombre();
        final double latitud = c.getLatitud();
        final double longuitud = c.getLonguitud();
        final double radio = c.getRadio();
        if (!archivo.exists()) {
            try {
                archivo.createNewFile();
                escribir = new FileWriter(archivo, true);
                linea = new PrintWriter(escribir);
                linea.println(
                    ide + ";" + nombre + ";" + latitud + ";" + longuitud + ";" +
                    radio + ";" + fecha + ";" + minerales);
                linea.close();
                escribir.close();
            } catch (final FileNotFoundException ex) {
                System.out.println("error");
            } catch (final IOException ex) {
                System.out.println("error");
            }
        } else {
            try {
                escribir = new FileWriter(archivo, true);
                linea = new PrintWriter(escribir);
                linea.println(
                    ide + ";" + nombre + ";" + latitud + ";" + longuitud + ";" +
                    radio + ";" + fecha + ";" + minerales);
                linea.close();
                escribir.close();
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Este metodo lee los archivos de las exploraciones
     *
     * @return @throws IOException
     */
    public static
    ArrayList<Exploracion> leerArchivoCrater() throws IOException
    {
        final var listaExploracion = new ArrayList<Exploracion>();
        try (final var bR = new BufferedReader(
            new FileReader(Constantes.Ruta.INFO.getRuta() + nombreArchiv,
                           StandardCharsets.UTF_8))) {
            String linea;
            String[] part;
            while ((linea = bR.readLine()) != null) {
                part = linea.split(";");
                listaExploracion.add(new Exploracion(
                    new Crater(part[0].trim(), part[1].trim(),
                               Double.parseDouble(part[2].trim()),
                               Double.parseDouble(part[3].trim()),
                               Double.parseDouble(part[4].trim())),
                    LocalDate.parse(part[5].trim()), part[6].trim()));
            }
        }
        return listaExploracion;
    }
}
