package control;

import utils.Constantes.Ruta;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;


public final
class CraterData {

    /* -------------------------------------------------------------------------
     * CONSTRUCTOR
     * ---------------------------------------------------------------------- */
    /* Prevenir instanciamiento */
    private
    CraterData() {}

    /**
     * Lee el archivo crater_info.txt y corta el texto segun un formato: id
     * archivo, nombre, pixeles, latitud y longitud, radio
     *
     * @return Arreglo de {@link Crater} con formato personalizado
     */
    public static
    ArrayList<Crater> cargarCrater() throws IOException
    {
        final var listaCrateres = new ArrayList<Crater>();
        try (final var bR = new BufferedReader(
            new FileReader(Ruta.INFO.getRuta() + "crateres_info.txt",
                           StandardCharsets.UTF_8))) {
            String linea;
            String[] part;
            while ((linea = bR.readLine()) != null) {
                part = linea.split(",");
                listaCrateres.add(new Crater(part[0].trim(), part[1].trim(),
                                             Double.parseDouble(part[2].trim()),
                                             Double.parseDouble(part[3].trim()),
                                             Double.parseDouble(
                                                 part[4].trim())));
            }
        }
        return listaCrateres;
    }
}