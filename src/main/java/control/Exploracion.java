package control;

import java.time.LocalDate;
import java.util.Random;


public
class Exploracion implements Comparable<Exploracion> {

    private final String[]  minerales       =
        {"Niquel", "Hierro", "Silicio", "Calcio", "Magnesio", "Aluminio",
         "Potasio", "Titanio", "Cromo", "Manganeso", "Azufre", "Fosforo",
         "Sodio", "Cloro", "Hidrogeno"};
    private final Random    rd              = new Random();
    private       Crater    crater;
    private       LocalDate fecha_exp;
    private       String    min_encontrados = "";

    public
    Exploracion(final Crater crater, final LocalDate fecha_exp)
    {
        this.crater = crater;
        this.fecha_exp = fecha_exp;
        generar_miner();
    }
    public
    void generar_miner()
    {
        final int n1 = rd.nextInt(3) + 1;
        int n2;
        for (int i = 0; i <= n1; i++) {
            n2 = rd.nextInt(15);
            min_encontrados += minerales[n2];
            if (i < n1) {
                min_encontrados += ", ";
            }
        }
    }

    public
    Exploracion(final Crater crater, final LocalDate fecha_exp,
                final String min_encontrados)
    {
        this.crater = crater;
        this.fecha_exp = fecha_exp;
        this.min_encontrados += min_encontrados;
    }
    public
    LocalDate getFecha_exp()
    {
        return fecha_exp;
    }
    public
    void setFecha_exp(final LocalDate fecha_exp)
    {
        this.fecha_exp = fecha_exp;
    }
    public
    String getMin_encontrados()
    {
        return min_encontrados;
    }
    public
    void setMin_encontrados(final String min_encontrados)
    {
        this.min_encontrados = min_encontrados;
    }
    @Override
    public
    int compareTo(final Exploracion e)
    {
        final int eCmp = crater.compareTo(e.getCrater());
        return eCmp;
    }
    public
    Crater getCrater()
    {
        return crater;
    }
    public
    void setCrater(final Crater crater)
    {
        this.crater = crater;
    }
    @Override
    public
    String toString()
    {
        return "Exploracion: " + "crater: " + crater + ", fecha_exp: " +
               fecha_exp + ", min_encontrados:" + min_encontrados + '}';
    }
}