package control;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.Objects;


public
class Crater implements Comparable<Crater> {

    private final String id;
    private final String nombre;
    private final double latitud;
    private final double longuitud;
    private final double radio;
    private final Circle circuloCrater;

    /* -------------------------------------------------------------------------
     * CONSTRUCTOR
     * ---------------------------------------------------------------------- */
    public
    Crater(final String id, final String nombre, final double latitud,
           final double longuitud, final double radio)
    {
        this.id = id;
        this.nombre = nombre;
        this.latitud = latitud;
        this.longuitud = longuitud;
        this.radio = radio;
        circuloCrater =
            new Circle(latitud, longuitud, radio, Color.TRANSPARENT);
        circuloCrater.setStroke(Color.RED);
        circuloCrater.setStrokeWidth(3);
    }

    @Override
    public final
    int hashCode()
    {
        return 31 * id.hashCode() + nombre.hashCode();
    }

    @Override
    public final
    boolean equals(final Object obj)
    {
        if (this == obj) { return true; }
        if (!(obj instanceof Crater)) { return false; }
        final Crater crater = (Crater) obj;

        return Objects.equals(id, crater.getId()) &&
               Objects.equals(nombre, crater.getNombre());
    }

    @Override
    public final
    String toString()
    {
        return String.format("'%s' || ID: '%s' || Radio: '%.3f'", nombre, id,
                             radio);
    }

    /* -------------------------------------------------------------------------
     * Getters & Setters
     * ---------------------------------------------------------------------- */
    final
    String getId() { return id; }
    public final
    String getNombre() { return nombre; }
    public final
    double getLatitud() { return latitud; }
    public final
    double getLonguitud() { return longuitud; }
    public final
    double getRadio() { return radio; }
    public final
    Circle getCirculoCrater() { return circuloCrater; }

    @Override
    public final
    int compareTo(final Crater t)
    {
        return nombre.compareTo(t.getNombre());
    }
}