package control;

interface Robot {
    /**
     * mueve al robot d pixeles hacia delante de la posición que se encuentra
     *
     * @param distancia distancia a recorrer (avanzar)
     */
    void avanzar(double distancia);

    /**
     * mueve al robot un valor predeterminado de pixeles pixeles hacia
     * delante de la posición que se encuentra
     */
    void avanzar();

    /**
     * Gira el robot los grados indicados en sentido de las manecillas del
     * robot. Si g es negativos se gira en contra del sentido de las manecillas
     * del robot.
     *
     * @param angulo grados a girar
     */
    void girar(double angulo);

    /**
     * Indica al rover que debe moverse a las coordenadas x,y que reciba como
     * parámetro. Internamente el rover debe buscar la ruta para llegar a la
     * ubicación y luego dirigirse a ella usando los comandos disponibles para
     * moverse que son avanzar y girar.
     *
     * @param posX coordenada en X a dirigirse
     * @param poxY coordenada en Y a dirigirse
     */
    void dirigirse(double posX, double posY);

    /**
     * Retorna un String con los minerales encontrados por el robot en el punto
     * actual separados por comas
     *
     * @return String con los minerales encontrados
     */
    void censar(Crater cr7);
}