package control;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ui.*;
import utils.Constantes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;

import static java.lang.StrictMath.*;
import static utils.Constantes.SYS_ERR_MSG;


public
class Rover implements Robot {

    private static final double    DIM_ROV_X      = 50;
    private static final double    LIM_IMG_X      =
        ExplorarUI.IMG_MARTE_WIDTH - DIM_ROV_X;
    private static final double    DIM_ROV_Y      = 50;
    private static final double    LIM_IMG_Y      =
        ExplorarUI.IMG_MARTE_HEIGHT - DIM_ROV_Y;
    private static final double    DESPLAZAMIENTO = 10;
    private final        ImageView imgRoverBox;
    private              double    rovPosX;
    private              double    rovPosY;
    private              boolean   rngRovX;
    private              boolean   rngRovY;

    public
    Rover() throws FileNotFoundException
    {
        this(DIM_ROV_X, DIM_ROV_Y);
    }

    /* -------------------------------------------------------------------------
     * CONSTRUCTOR
     * ---------------------------------------------------------------------- */
    public
    Rover(final double dimRovX, final double dimRovY)
        throws FileNotFoundException
    {
        final var imgRover = new Image(
            new FileInputStream(Constantes.Ruta.IMG.getRuta() + "rover.png"),
            dimRovX, dimRovY, true, true);
        imgRoverBox = new ImageView(imgRover);
    }

    /* -------------------------------------------------------------------------
     * Getters & Setters
     * ---------------------------------------------------------------------- */
    public final
    ImageView getImgRoverBox() { return imgRoverBox; }

    /**
     * Gira el robot los grados indicados de forma progresiva (se suman) en
     * sentido de las manecillas del robot. Si g es negativos se gira en
     * contra del sentido de las manecillas del robot.
     *
     * @param angulo grados a girar
     */
    public final
    void girarProgresivo(final double angulo)
    {
        imgRoverBox.setRotate(imgRoverBox.getRotate() + angulo);
    }

    /**
     * Calcula el angulo con respecto  a la horizontal para que el Rover se
     * mueva en direccion correcta
     *
     * @param posX Posicion solicitada en el eje X
     * @param posY Posicion solicitada en el eje Y
     *
     * @return angulo
     */
    final
    double calcularAngulo(final double posX, final double posY)
    {
        final var diffX = posX - imgRoverBox.getLayoutX();
        final var diffY = posY - imgRoverBox.getLayoutY();
        var angulo = toDegrees(abs(atan(diffY / diffX)));

        /* Calcular el angulo de giro */
        if (diffX >= 0 && diffY >= 0) {
            return angulo;
        } else if (diffX < 0 && diffY >= 0) {
            angulo = 180 - angulo;
        } else if (diffX < 0 && diffY < 0) {
            angulo += 180;
        } else if (diffX >= 0 && diffY < 0) {
            angulo = 360 - angulo;
        }

        return angulo;
    }

    @Override
    public final
    void avanzar(final double distancia)
    {
        /* Cargar configuracion */
        configuracion();

        /* Calcular la distancia dependiendo del angulo */
        final double dx = distancia * cos(toRadians(imgRoverBox.getRotate()));
        final double dy = distancia * sin(toRadians(imgRoverBox.getRotate()));

        /* Verificar que el Rover no exceda los limites del Pane contenedor
         * de la imagen de Marte
         */
        /* Si esta adentro del rango de X OR Y */
        if (rngRovX || rngRovY) {
            /* Si esta adentro del rango de X AND Y */
            if (rngRovX && rngRovY) {
                desplazar('0', dx, dy);
                /* Si el rango es excedido en X OR Y */
            } else if (rovPosX >= LIM_IMG_X || rovPosY >= LIM_IMG_Y) {
                /* Si el rango es excedido en X */
                if (rovPosX >= LIM_IMG_X) {
                    desplazar('0', -1, dy);
                    /* Si el rango es excedido en Y */
                } else if (rovPosY >= LIM_IMG_Y) {
                    desplazar('0', dx, -1);
                }
            } else {
                dirigirse(0, 0);
            }
        } else {
            dirigirse(0, 0);
        }
    }

    @Override
    public final
    void avanzar()
    {
        avanzar(DESPLAZAMIENTO);
    }

    @Override
    public final
    void girar(final double angulo)
    {
        imgRoverBox.setRotate(angulo);
    }

    @Override
    public final
    void dirigirse(final double posX, final double posY)
    {
        /* Cargar configuracion */
        configuracion();

        /* Si esta adentro del rango de X OR Y */
        if (rngRovX || rngRovY) {
            /*
             * Si la posicion en X AND Y estan adentro de los limites de la
             * imagen */
            if (posX < LIM_IMG_X && posY < LIM_IMG_Y) {
                /* Girar a direccion correspondiente */
                girar(calcularAngulo(posX, posY));
                /* Desplazarse distancia solicitada */
                final var distancia =
                    PlanificarUI.calcularDistancia(imgRoverBox.getLayoutX(),
                                                   imgRoverBox.getLayoutY(),
                                                   posX, posY);
                final var repeticiones = (int) (distancia / DESPLAZAMIENTO);

                /* Hilo que simula la animacion del movimiento del Rover */
                new Thread(() -> {
                    try {
                        for (int i = 0; i < repeticiones; ++i) {
                            Platform.runLater(this::avanzar);
                            Thread.sleep(250);
                        }
                    } catch (final InterruptedException iEx) {
                        ErrorUI.show();
                        SYS_ERR_MSG(iEx.getMessage());
                    }
                }).start();
            }
        }
    }

    @Override
    public final
    void censar(final Crater cr7)
    {
        final var exploracion = new Exploracion(cr7, LocalDate.now());
        ExploracionData.crearArchivoCrater(exploracion);
    }

    /**
     * Carga valores esenciales del Rover para ejecutar distintas acciones
     * - Posicion X/Y
     * - Rango del Rover adentro de los limites de la imagen
     */
    private
    void configuracion()
    {
        rovPosX = ExplorarUI.bender.imgRoverBox.getLayoutX();
        rovPosY = ExplorarUI.bender.imgRoverBox.getLayoutY();
        rngRovX = rovPosX >= 0 && rovPosX <= LIM_IMG_X;
        rngRovY = rovPosY >= 0 && rovPosY <= LIM_IMG_Y;
    }

    /**
     * Realiza la accion de desplazar el Rover en base a su posicion en el
     * momento de ejecucion del comando
     *
     * @param eje Eje X o Y
     * @param dN  Arreglo que recibe el desplazamiento sea en X, Y o ambos
     */
    private
    void desplazar(final char eje, final double... dN)
    {
        switch (eje) {
        case 'x':
            imgRoverBox.setLayoutX(imgRoverBox.getLayoutX() + dN[0]);
            break;
        case 'y':
            imgRoverBox.setLayoutY(imgRoverBox.getLayoutY() + dN[0]);
            break;
        default:
            imgRoverBox.setLayoutX(imgRoverBox.getLayoutX() + dN[0]);
            imgRoverBox.setLayoutY(imgRoverBox.getLayoutY() + dN[1]);
            break;
        }
    }
}